int r = 9;
int b = 11;
int v = 10;

void setup() {
  Serial.begin(9600);

  while (!Serial) {
    delay(10);
  }

  pinMode(r, OUTPUT);
  pinMode(v, OUTPUT);
  pinMode(b, OUTPUT);
  delay(250);
}

int read_serial;
String content = "";
char character;
const char delimiter = ";";
long rouge;
char *token; 

void loop() {

  while (Serial.available()) {
    content = Serial.readString();
    delay(3);
  }

  if (content.equals("NAME"))
  {
    Serial.println("LUMIERE");
    content == "";
    delay(500);
  }

  if (content != "") {
    int colors [3];
    char *tmp;
    
    Serial.println(content);

    int i = 0;
    tmp = strtok((char*)content.c_str(), ";");
    colors[i] = atoi(tmp);
    i++;

    while(i < 3) {
      tmp = strtok((char*)NULL, ";");
      Serial.println( (String)i + " : " + (String)tmp);
      colors[i] = atoi(tmp);
      i++;
    }

    //Serial.println(colors[0]);
    //Serial.println(colors[1]);
    //Serial.println(colors[2]);

    if (rouge >= 0 and rouge <= 255) {
      analogWrite(r, 255 - colors[0]);
      analogWrite(v, 255 - colors[1]);
      analogWrite(b, 255 - colors[2]);
    }
    content = "";
    delay(1000); 
  }
}
