# Import des modules
import json

# Nom du fichier
json_file = "data.json"

def write_json(arg, val):
    """
    Permet d'écrire dans le json
    """
# On ouvre le json
    with open(json_file, 'r') as f:
        data = json.load(f)

# On récupère la valeur
    data[arg] = val

# On écrire dans le json
    with open(json_file, 'w') as f:
        json.dump(data, f)


def read_json(arg):
    """
    Permet de récupérer une valeur dans le json
    """
    # Ouvre le fichier
    with open(json_file, 'r') as f:
        data = json.load(f)
    # Retourne la valeur
    return data.get(arg)

def read_json_id(json_file):
    """
    Peremt de récupérer les infos venant des json id
    """
    # Ouvre le fichier
    with open(json_file, 'r') as f:
        data = json.load(f)
    # Retourne un dico
    return data
