# Import du module
from mpd import MPDClient

def get_mpd():
    """
    Permet de récupérer un objet mpd
    """
    client = MPDClient()               # create client object
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6600)  # connect to localhost:6600
    return client


def toggle():
    """
    Permet de changer l'état de pause
    """
    client = get_mpd()
    status = client.status()
    if status['state'] == 'pause':
        client.pause(0)
    elif status['state'] == 'play':
        client.pause(1)
