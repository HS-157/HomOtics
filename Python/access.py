# Import des modules
import os.path
from json_tools import read_json_id

def access(id_card):
    """
    Permet de renvoyer si l'id à accès et les infos lié à cette id
    """

    # Nom du json
    file_id = id_card + ".json"
    # On vérifie s'il existe
    if os.path.exists(file_id):
        # On récupère les infos du json
        data = read_json_id(file_id)

        # On récupère les clefs
        name = data["name"]
        color = data["color"]
        color_list = color.split(";")
        msg = data["msg"]
        access = data["access"]
        # On converti en booléen
        if access == 'True':
            access_bool = True
        elif access == 'False':
            access_bool = False
        else:
            raise ValueError
        id_card_json = data["id"]
        # On retourne un tableau de valeur
        return [bool(access_bool), name, color_list, msg, id_card_json]

    else:
        return [False] * 5
