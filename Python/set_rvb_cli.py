# Import les modules
from serial import *
import json
from time import sleep

import argparse

from set_rvb import set_rvb

"""
Permet d'avoir une interface CLI pour mettre la couleur
"""


# Créer un objet parser
parser = argparse.ArgumentParser()

# On définie les paramètres
parser.add_argument("-r", "--rouge", help="Couleur rouge", required=True)
parser.add_argument("-v", "--vert", help="Couleur vert", required=True)
parser.add_argument("-b", "--bleu", help="Couleur bleu", required=True)

# Si le parser n'est pas vide
if parser is not None:
    # On récupère les paramètres
    args = parser.parse_args()
    rouge = args.rouge
    vert = args.vert
    bleu = args.bleu

    # On lance la fonction
    set_rvb(rouge, vert, bleu)

