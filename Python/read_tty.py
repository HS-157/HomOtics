# Import les modules
from serial import *
from random import *
from time import sleep
from set_rvb import set_rvb
from json_tools import write_json

def read_tty():
    """
    Permet de savoir quel Arduino est sur quel port
    """

    # TL;DR : Envoie un message à Arduino pour savoir son id, et grâce à ça on peut savoir quel Arduino est sur quel port

    l = [b'LUMIERE\r\n', b'CARTE\r\n']
    
    serial_ACM0 = Serial(port="/dev/ttyACM0", baudrate=9600, timeout=1, writeTimeout=1)
    
    while not(serial_ACM0.isOpen()):
        pass
    
    line_ACM0 = serial_ACM0.readline()
    
    while(line_ACM0 not in l):
        serial_ACM0.write(b"NAME")
        sleep(randrange(5))
        if line_ACM0 not in l:
            line_ACM0 = serial_ACM0.readline()
            print("read 1 : ", line_ACM0)
    
    if line_ACM0 == b"LUMIERE\r\n":
        serial_light = serial_ACM0
        led = "/dev/ttyACM0"
        print("Light : ACM0")
    
    elif line_ACM0 == b"CARTE\r\n":
        serial_carte = serial_ACM0
        print("Carte : ACM0")
        carte = "/dev/ttyACM0"
    
    
    serial_ACM1 = Serial(port="/dev/ttyACM1", baudrate=9600, timeout=1, writeTimeout=1)
    
    while not(serial_ACM1.isOpen()):
        pass
    
    line_ACM1 = serial_ACM1.readline()
    
    while(line_ACM1 not in l):
        serial_ACM1.write(b"NAME")
        sleep(randrange(5))
        if line_ACM1 not in l:
            line_ACM1 = serial_ACM1.readline()
            print("read 2 : ", line_ACM1)
    
    if line_ACM1 == b"LUMIERE\r\n":
        serial_light = serial_ACM1
        led = "/dev/ttyACM1"
        print("Light : ACM1")
    
    elif line_ACM1 == b"CARTE\r\n":
        serial_carte = serial_ACM1
        print("Carte : ACM1")
        carte = "/dev/ttyACM1"
    
    
    serial_light.close()
    serial_carte.close()
    
    write_json('light', led)
    write_json('card', carte)
