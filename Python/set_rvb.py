# Import des modules
from serial import *
import json
from time import sleep

def set_rvb(r, v, b):
    """
    Permet de mettre la couleur pour la led
    """

    # On ouvre le fichier
    with open('data.json', 'r') as f:
        data = json.load(f)      
    
    # On récupère le port
    dev = data.get("light")
    
    with Serial(port=dev, baudrate=9600, timeout=1, writeTimeout=1) as port:
        # Si ouvert
        if port.isOpen():
            # On attend
            sleep(2)
            # On affiche les valeurs
            print("Couleur - R :", r, " V : ", v, " B : ", b)
            # On regroupe
            color = str(r) + ";" + str(v) + ";" + str(b)
            # On convertie et encode
            port.write(bytes(color, encoding='utf-8'))

            # On met les valeur dans le dico
            data["r"] = r
            data["g"] = v
            data["b"] = b

            # On écrit dans le json
            with open('data.json', 'w') as f:
                json.dump(data, f)      
