# Import des modules
from serial import *
from time import sleep
from set_rvb import set_rvb
from json_tools import read_json
from client_mpd import toggle
from access import access

def card_reader():
    """
    Fonction principale qui lit les infos envoyé par le lecteur de carte
    et renvoie ce qu'il faut afficher sur l'écran lcd
    """
    # On récupère le chemin du port série
    carte = read_json("card")
    with Serial(port=carte, baudrate=9600, timeout=1, writeTimeout=1) as port:
        # On vérifie s'il est ouvert
        if port.isOpen():
            # Boucle infini
            while True:
                # On récupère les infos venant du port et converti en String
                ligne = str(port.readline(), 'utf-8')
                # Affiche ce qu'il vient du port série
                print("Serial :", ligne)
                # S'il la chaîne de caractère n'est pas vide
                if ligne != "":
                    # Si elle contient "SCAN"
                    if "SCAN" in ligne:
                        # On découpe la chaîne et récupère l'id
                        split_ligne = ligne.split(" ")
                        id_card = split_ligne[1].replace("\r\n", "")

                        # On récupère les infos
                        info_id = access(id_card)

                        # S'il a accès
                        if info_id[0]:
                            color = info_id[2]
                            # Le message à envoyer
                            msg = info_id[3]
                            # On joue / pause la musique
                            toggle()
                            # On change la couleur
                            set_rvb(color[0], color[1], color[2])

                        else:
                            # Si pas accès on envoie ce message
                            msg = "Access denied"
    
                    # Si on reçoit depuis le détecteur de porte ouverte
                    elif "6106473" in ligne:
                        msg = "Alarme porte"

                    # On convertie en bits
                    msg = bytes(msg, encoding='utf-8')
                    # Et envoie
                    port.write(msg)
                sleep(0.25)
    
