# Import des fonctions
from read_tty import read_tty
from card_reader import card_reader

# Lance le programme
if __name__ == '__main__':
    read_tty()
    card_reader()
