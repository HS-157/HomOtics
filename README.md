# EPSI_B3_Workshop

Projet réalisé durant le Workshop B3 d'avril 2017 à l'EPSI de Brest à Brest Open Campus


## Dépendances

Système :

- [Python 3](https://www.python.org/)
- [MPD](https://www.musicpd.org/)
- [Arduino IDE](https://www.arduino.cc/en/main/software)

Python :

- [python-mpd2](https://github.com/Mic92/python-mpd2)
- [pyserial](https://pythonhosted.org/pyserial/)

Arduino :

- [Arduino/libraries](Arduino/libraries)


## Présentation

- [https://prezi.com/w_fwaxwmv5bh/hom039otics/](https://prezi.com/w_fwaxwmv5bh/hom039otics/)

## Image

![RFID](doc/rfid.jpg)

---


## Contributeur

- [SegalenPh](https://github.com/SegalenPh)
- [tjezequel](https://github.com/tjezequel)
- [HS-157](https://github.com/HS-157)
